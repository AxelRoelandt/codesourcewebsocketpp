
// Import required libraries
 #include <stdint.h>
  #include <Arduino.h>
  #include <ESP8266WiFi.h>  
  #include <ESP8266WiFiMulti.h>    
  #include <Hash.h>
  #include <ESPAsyncTCP.h>
  #include <ESPAsyncWebServer.h>
  #include <FS.h>
 
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
 #include <WebSocketClient.h>

#define USE_SERIAL Serial1

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;     

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);


// Replace with your network credentials
const char* ssid = "guest";
const char* password = "";
WebSocketClient webSocketClient;
// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

String readBME280Temperature() {
   sensors.requestTemperatures(); 
  // Read temperature as Celsius (the default)
 float temperatureC = sensors.getTempCByIndex(0);
  // Convert temperature to Fahrenheit
  //t = 1.8 * t + 32;
  if (isnan(temperatureC)) {    
    Serial.println("Failed to read from  sensor!");
    return "";
  }
  else {
    Serial.println(temperatureC);
    return String(temperatureC);
  }
}

  

void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  
  

	 

  // // Initialize SPIFFS
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP32 Local IP Address
  
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", readBME280Temperature().c_str());
  });
  // server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request){
  //   request->send_P(200, "text/plain", readBME280Humidity().c_str());
  // });
  // server.on("/pressure", HTTP_GET, [](AsyncWebServerRequest *request){
  //   request->send_P(200, "text/plain", readBME280Pressure().c_str());
  // });

  // Start the DS18B20 sensor
  sensors.begin();

  // Start server
  server.begin();

 

}
 
void loop(){
  String data;
 
  if (webSocketClient.connect("http://10.10.16.201","/signalr", 80)) {
 
    data = readBME280Temperature();
    webSocketClient.send(data);
    if (data.length() > 0) {
      Serial.print("Received data: ");
      Serial.println(data);
    }
 
  } else {
    Serial.println("Client disconnected.");
  }
 
  delay(3000);
}